import { IsArray, IsEnum, IsNumber, IsString } from 'class-validator';
import { Roles } from '../../common/enum';

export class Subjects {
  @IsString()
  name: string;
  @IsString()
  type: string;
  @IsNumber()
  labScore: number;
  @IsNumber()
  theoreticalPracticalScore: number;
}

class Rol {
  @IsEnum(Roles)
  name: string;
}

export class UserDto {
  @IsNumber()
  userId: number;
  @IsArray()
  role: [Rol];
  @IsString()
  name: string;
  @IsArray()
  subjects: [Subjects];
}
