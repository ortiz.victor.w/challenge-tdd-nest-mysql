import { Body, Controller, Post, UsePipes } from '@nestjs/common';
import { UserDto } from './dto/student.dto';
import { StudentService } from './student.service';

@Controller('student')
export class StudentController {
  constructor(private studentService: StudentService) {}

  @Post()
  public saveUser(@Body() user: UserDto) {
    this.studentService.save(user);
  }
}
