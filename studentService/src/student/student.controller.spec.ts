import { Test, TestingModule } from '@nestjs/testing';
import { UserDto } from './dto/student.dto';
import { StudentController } from './student.controller';
import { StudentService } from './student.service';
jest.mock('./student.service.ts');

describe('StudentController', () => {
  let controller: StudentController;
  let service: StudentService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [StudentController],
      providers: [StudentService],
    }).compile();

    controller = module.get<StudentController>(StudentController);
    service = module.get<StudentService>(StudentService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should call the saveUser service', () => {
    const newUser: UserDto = {
      userId: 1,
      name: 'Victor',
      role: [{ name: 'STUDENT' }],
      subjects: [
        {
          name: 'CHEMICAL',
          labScore: 8,
          theoreticalPracticalScore: 7,
          type: 'LABORATORY',
        },
      ],
    };
    controller.saveUser(newUser);
    expect(service.save).toHaveBeenCalled();
  });
});
